<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRABLA DE PREGUNTAS EDITABLES</title>
</head>
<body background="imagenes/imag7.jpg">
    <center>
    <h1>TRABLA DE PREGUNTAS</h1></br>
    <table border="3">
         <thead>
            <tr>
                <th colspan="1"><a href="indexadmi.php"><h2>Nuevo</h2></a></th>
            </tr>
        </thead>
        <tbody>
             <tr>
             <td><h3>Nª PREG.</h3></td>
             <td><h3>PREGUNTA</h3></td>
             <td><h3>RESPUESTA (a)</h3></td>
             <td><h3>RESPUESTA (b)</h3></td>
             <td><h3>RESPUESTA (c)</h3></td>
             <td><h3>RESPUESTA (d)</h3></td>
             <td><h3>CORRECTO</h3></td>
             <td><h3>MODIFICAR</h3></td>
             <td><h3>ELIMINAR</h3></td>
             </tr>
             <?php
             include("conexion.php");
             $query="SELECT * FROM preguntas";
             $resultado= $conexion -> query($query);
             while($row=$resultado->fetch_assoc()){
                 $id = $row['id'];
            ?>
                <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['pregunta']; ?></td>
                <td><?php echo $row['resp1']; ?></td>
                <td><?php echo $row['resp2']; ?></td>
                <td><?php echo $row['resp3']; ?></td>
                <td><?php echo $row['resp4']; ?></td>
                <td><?php echo $row['correcto']; ?></td>
                <td><a href="modificar.php?id=<?php echo $row['id'];?>">Modificar</a></td>
                <?php echo "<td><a href='eliminaradmi.php?id=$id'>Eliminar</a></td>";?>
                </tr>
            <?php
             }
             ?>
        </tbody>
    </table>
    </center>
</body>
</html>
<!-- body {
	background-image: url(imagenes/imagenes/43160hd.jpg);
}
-->